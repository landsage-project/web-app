import React, { Component } from 'react'
import { Marker, Popup } from 'react-leaflet';
import { Divider } from 'antd';
import { Row, Col } from 'react-bootstrap';
import Leaflet from 'leaflet';

const API_URL = 'https://landsage.app:1337/api/v1'
const GRAPH_URL = 'https://landsage.app:3006'

export const BeaconMarker = ({
    position,
    sortedTimeSeriesData,
    onClick,
    markerRef,
    isUnSync,
    data,
    site,
    vizPath,
    icon,
    name
}) => {
    return (
        <Marker position={position} icon={icon} eventHandlers={onClick} ref={markerRef}>
            <Popup>
                <div
                    style={{
                        width: 280,
                        height: 'auto'
                    }}>
                    <h6>{`Station : ${data.areaName}`}</h6>
                    {sortedTimeSeriesData ? Object.keys(sortedTimeSeriesData).map(item => (
                        <Row>
                            <Col xs={4}>
                                <div
                                    style={{
                                        fontWeight: 'bold'
                                    }}>{`${item}:`}</div>
                            </Col>
                            <Col>
                                <div
                                    style={{
                                        marginLeft: 4
                                    }}>{`${sortedTimeSeriesData[item]}`}</div>
                            </Col>
                        </Row>
                    ))
                        : null
                    }
                    {data.fileUrl
                        ? (
                            <div>
                                <Divider />
                                <div>{'File List'}</div>
                                <Row>
                                    <Col>
                                        <a
                                            style={{ wordWrap: 'break-word', color: '#693' }}
                                            onClick={() => {
                                                window.open(`${API_URL}/data/csv/${name}.csv`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                            }}>{name}</a>
                                    </Col>
                                    <Col style={{ marginTop: 12 }}>
                                        <a
                                            href="javascript:void(0)"
                                            style={{ marginTop: 12 }}
                                            onClick={() => {
                                                window.open(`${GRAPH_URL}/${data.stationId}/${data.typeId}?site=${site}`, "_blank", 'location=yes,height=640,width=480,scrollbars=yes,status=yes');
                                                if (isUnSync)
                                                    return;
                                                //emitter();
                                            }}>{'More Details'}</a>
                                    </Col>
                                </Row>
                            </div>
                        )
                        : null}
                </div>
            </Popup>
        </Marker>
    )
}