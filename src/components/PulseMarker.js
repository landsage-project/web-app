import React, { Component } from 'react';
import Leaflet from 'leaflet'
import { Marker } from 'react-leaflet';

class PulseMarker extends Component {
    state = {
        enable : true,
    }
    componentDidMount () {
        setTimeout(() => {
            this.setState({enable: false});
        },4000)
    }
    render() {
        const { position, onClick, markerRef } = this.props
        return this.state.enable && <Marker position={position} icon={PulseIcon} eventHandlers={onClick} ref={markerRef} />

    }
}

export default PulseMarker;

export const PulseIcon = new Leaflet.divIcon({
    html:
        `<svg width="40" height="40" viewbox="0 0 40 40" xmlns="http://www.w3.org/2000/svg">
        <circle cx="20" cy="20" fill="none" r="10" stroke="#ff4f81" stroke-width="2">
          <animate attributeName="r" from="8" to="20" dur="2.0s" begin="0s" repeatCount="indefinite"/>
          <animate attributeName="opacity" from="0" to="1" dur="2.0s" begin="0s" repeatCount="indefinite"/>
          <animate attributeName="opacity" from="1" to="0" dur="2.0s" begin="0s" repeatCount="indefinite"/>
        </circle>
        <circle cx="20" cy="20" fill="#ff4f81" r="10">
          <animate attributeName="r" from="8" to="20" dur="2.0s" begin="0s" repeatCount="indefinite"/>
          <animate attributeName="opacity" from="0" to="1" dur="2.0s" begin="0s" repeatCount="indefinite"/>
          <animate attributeName="opacity" from="1" to="0" dur="2.0s" begin="0s" repeatCount="indefinite"/>
        </circle>
        
      </svg>`, className: '', iconSize: [
        40, 40
    ],
})