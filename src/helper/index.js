import React, { Component } from 'react'
import Leaflet from 'leaflet';
import moment from 'moment';
import axios from 'axios';

import mapIcon from '../asset/map-pin.png'
import rainIconSVG from '../asset/rain_green.svg'
import ubonLandusePic from "../asset/ubon-landuse-2017.png"
import ubonPopulationPic from "../asset/ubon-population-2017.png"
import mrcbound from '../data/mrcbound.json'
import mrcmekong from '../data/mrcmekong.json'

const API_URL = 'https://landsage.app:1337/api/v1'

export const dischargeIcon = (fill) => new Leaflet.divIcon({
    html: `<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24">
        <g>
            <path xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_2" d="m0,11.86945l12.49178,-11.86945l12.49178,11.86945l-12.49178,11.86945l-12.49178,-11.86945z" fill="${fill || '#06F'}"/>
        </g>
        </svg>`,
    className: '',
    iconSize: [
        25, 24
    ],
    iconAnchor: [
        12.5, 24
    ],
    popupAnchor: [0, -28]
});


export const requestForecastDataFromServer = async (urlDataHistorical, urlData) => {

    var data = new FormData();
    data.append('urlDataHistorical', urlDataHistorical);
    data.append('urlData', urlData);

    var config = {
      method: 'post',
      url: `https://landsage.app:8000/getForecast7DaysOfLGB`,
      data: data
    };
    const response = await axios(config);
    if (response && response.data && response.data.data) {
        // console.log("*****************response.data.data = ", response.data.data);
        return response.data.data;
        
    }

  };

  export const requestLandslideRiskFromServer = async (list_rainfall_raw) => {

    console.log('list_rainfall_raw = ' ,list_rainfall_raw);
    let list_rainfall = [];
    list_rainfall_raw.forEach(rainfall_data => {
        // console.log("rainfall_data = ", rainfall_data);
        list_rainfall.push({
            "rain_level": rainfall_data['rain_24h'],
            "lat": rainfall_data['position'][0],
            "lon": rainfall_data['position'][1],
            "isRisk": false
        })
    });

    // let val =     [
    //     {
    //         "rain_level": 100,
    //         "lat": 19.393637,
    //         "lon": 100.45059,
    //         "isRisk": false
    //     },
    //     {
    //         "rain_level": 200,
    //         "lat": 19.393637,
    //         "lon": 100.45059,
    //         "isRisk": false
    //     }
    // ];

    // const url = 'http://18.141.239.234:8001/getLandslideRisk';
    // const url = 'http://127.0.0.1:5000/getLandslideRisk';
    const url = `https://landsage.app:8001/getLandslideRisk`;
    
  
    let isDebugRiskLandslide = sessionStorage.getItem("isDebugRiskLandslide") === 'true' ? true: false;

    const requestData = { 
        'list_rain_data': list_rainfall,
        'isDebug':isDebugRiskLandslide
    };

    // const data = val.filter(function(item) {
    //     return true;
    //   }).map(function(item) {

    //     if(isDebugRiskLandslide){
    //         item.isRisk = true;
    //     }

    //     return {
    //         name: "Landslide Assessment",
    //         rain_level: item.rain_level,
    //         isRisk: item.isRisk,
    //         position: [
    //             item.lat, item.lon
    //         ],
    //         ...item
    //     }
    // });

    // return data;

    try {
        const response = await axios.post(url, requestData, {
            headers: {
              'Content-Type': 'application/json'
            }
          });
        //   const response = {
        //     "data": [
        //         {
        //             "rain_level": 200,
        //             "lat": 19.393637,
        //             "lon": 100.45059,
        //             "isRisk": true
        //         },
        //     ]
        //   };

      if (response) {
        // console.log(response.data);

        const data = response.data.filter(function(item) {
            return true;
        }).map(function(item) {
            // console.log("********item.lat, item.lon, ", item.lat, item.lon)
          if(isDebugRiskLandslide && item.lat == 19.393637 && item.lon == 100.45059){
              item.isRisk = true;
          }
  
          return {
              name: "Landslide Assessment",
              rain_level: item.rain_level,
              isRisk: item.isRisk,
              position: [
                  item.lat, item.lon
              ],
              ...item
          }
      });

        return data;

      }
    } catch (error) {
      console.error(error);
      return [];
    }

  };


export const waterLevelForecastAlarmIcon = async (value, floodLevel = 0, alarmLevel = 0, urlDataHistorical = "", urlData = "") => {
// export const waterLevelForecastAlarmIcon = (value) => {  
    // value = '#0f0'; // no warning
    // value = '#fa0'; // alarm situation
    // value = '#f30'; // flood stage
    let isDebugFlood = sessionStorage.getItem("isDebugFlood") === 'true' ? true: false;
    

    let alertFloodIcon = `
    <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
    width="20" height="20" viewBox="0 0 512.000000 512.000000"
    preserveAspectRatio="xMidYMid meet" style="position: absolute; left: 3px; top: 2px;">
        <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
        fill="#F35533" stroke="#000">
        <path stroke-width="300" d="M2520 4823 c-202 -14 -410 -119 -532 -267 -56 -68 -1879 -3111 -1922
        -3208 -47 -105 -60 -174 -60 -308 0 -115 3 -133 33 -220 73 -215 191 -350 386
        -445 162 -78 15 -72 2075 -80 1301 -5 1872 -3 1937 4 194 23 329 87 458 216
        84 84 131 154 172 255 81 202 67 436 -40 636 -17 34 -441 747 -942 1585 l-910
        1524 -85 85 c-47 47 -111 100 -143 119 -79 46 -201 88 -279 96 -34 3 -72 7
        -83 9 -11 1 -40 1 -65 -1z m167 -951 c63 -33 111 -79 141 -136 l27 -51 3 -669
        c3 -751 5 -726 -70 -810 -61 -70 -116 -98 -203 -104 -129 -8 -234 50 -293 162
        l-27 51 0 685 0 685 30 54 c36 65 92 114 159 141 67 27 172 23 233 -8z m-35
        -2083 c285 -60 439 -372 314 -635 -108 -228 -371 -320 -601 -211 -279 134
        -342 501 -124 720 110 110 264 157 411 126z"/>
        </g>
    </svg>
    `;

    let isAlarm = false;
    let color = '';
    if(urlDataHistorical === "" || urlData === ""){
        alertFloodIcon = '';
    }else{

        const forecastWaterLevel = await requestForecastDataFromServer(urlDataHistorical, urlData);
        // console.log("++++++++++++++forecastWaterLevel = ", forecastWaterLevel);
        for (let index = 0; index < forecastWaterLevel.length; index++) {
            const waterLevelInTheFuture = forecastWaterLevel[index];

            if (waterLevelInTheFuture >= alarmLevel && waterLevelInTheFuture < floodLevel) {
                color = '#FFEB00';
                isAlarm = true;
                break;
            }else if(waterLevelInTheFuture >= floodLevel || isDebugFlood){
            // }else if(waterLevelInTheFuture >= floodLevel || waterLevelInTheFuture < 3){
                color = '#F35533';
                isAlarm = true;
                break;
            }
        }
    }

    // if(alarmLevel === 8){
    //     value = '#f30'
    //     color = '#F35533';
    //     isAlarm = true;
    // }


    if(urlDataHistorical === "" || urlData === ""){
        alertFloodIcon = '';
    }else{
        alertFloodIcon = `
            <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
            width="20" height="20" viewBox="0 0 512.000000 512.000000"
            preserveAspectRatio="xMidYMid meet" style="position: absolute; left: 3px; top: 2px;">
                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
                fill="${color}" stroke="#000">
                <path stroke-width="300" d="M2520 4823 c-202 -14 -410 -119 -532 -267 -56 -68 -1879 -3111 -1922
                -3208 -47 -105 -60 -174 -60 -308 0 -115 3 -133 33 -220 73 -215 191 -350 386
                -445 162 -78 15 -72 2075 -80 1301 -5 1872 -3 1937 4 194 23 329 87 458 216
                84 84 131 154 172 255 81 202 67 436 -40 636 -17 34 -441 747 -942 1585 l-910
                1524 -85 85 c-47 47 -111 100 -143 119 -79 46 -201 88 -279 96 -34 3 -72 7
                -83 9 -11 1 -40 1 -65 -1z m167 -951 c63 -33 111 -79 141 -136 l27 -51 3 -669
                c3 -751 5 -726 -70 -810 -61 -70 -116 -98 -203 -104 -129 -8 -234 50 -293 162
                l-27 51 0 685 0 685 30 54 c36 65 92 114 159 141 67 27 172 23 233 -8z m-35
                -2083 c285 -60 439 -372 314 -635 -108 -228 -371 -320 -601 -211 -279 134
                -342 501 -124 720 110 110 264 157 411 126z"/>
                </g>
            </svg>
            `;
        if(!isAlarm){
            alertFloodIcon = '';
        }
    }

    return new Leaflet.divIcon({
        html: `
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="24" style="position: absolute;">
                <g>
                    <rect xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_4" height="22.98852" width="23.90806" y="0.3908" x="0.54596" fill="${value}"/>
                </g>
            </svg>
            ${alertFloodIcon}
        </div>`,

        className: '',
        iconSize: [
            25, 24
        ],
        iconAnchor: [
            12.5, 24
        ],
        popupAnchor: [0, -28]
    });
}


export const waterLevelIcon = (value) => {

    return new Leaflet.divIcon({
        html: `<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24">
            <g>
            <rect xmlns="http://www.w3.org/2000/svg" stroke="#000" id="svg_4" height="22.98852" width="23.90806" y="0.3908" x="0.54596" fill="${value}"/>
            </g>
            </svg>`,
        className: '',
        iconSize: [
            25, 24
        ],
        iconAnchor: [
            12.5, 24
        ],
        popupAnchor: [0, -28]
    });
}


export const customIcon = new Leaflet.Icon({
    iconUrl: mapIcon,
    iconSize: [
        23, 32
    ],
    iconAnchor: [
        11.5, 32
    ],
    popupAnchor: [0, -34]
});

export const waterNHDCIcon = (percent, water, waterPrev) => {
    let iconName = 'no_data'
    let folder = 'rectangle';

    if (percent <= 10) {
        iconName = 'brown'
    } else if (percent > 10 && percent <= 30) {
        iconName = 'yellow'
    } else if (percent > 30 && percent <= 70) {
        iconName = 'green'
    } else if (percent > 70 && percent <= 100) {
        iconName = 'blue'
    } else if (percent > 100) {
        iconName = 'red'
    }

    if(water > waterPrev){
        folder = 'up';
    }else if(water < waterPrev){
        folder = 'down';
    }else{
        folder = 'rectangle';
    }

    return new Leaflet.Icon({
        iconUrl: iconName === 'no_data' ? require(`../asset/${iconName}.svg`).default : 
                                            require(`../asset/iconWaterLevelNHDC/${folder}/${iconName}.png`).default,
        iconSize: [
            20, 20
        ],
        iconAnchor: [
            10, 20
        ],
        popupAnchor: [0, -22]
    });
}

export const rainNHDCIcon = (rain) => {
    let iconName = 'no_data'

    if (rain <= 10) {
        iconName = 'blue';
    } else if (rain > 10 && rain <= 20) {
        iconName = 'celadon';
    } else if (rain > 20 && rain <= 35) {
        iconName = 'green';
    } else if (rain > 35 && rain <= 50) {
        iconName = 'yellow';
    }else if (rain > 50 && rain <= 70) {
        iconName = 'orange';
    }else if (rain > 70 && rain <= 90) {
        iconName = 'brown';
    } else if (rain > 90) {
        iconName = 'red';
    }

    return new Leaflet.Icon({
        iconUrl: iconName === 'no_data' ? require(`../asset/${iconName}.svg`).default : 
                                            require(`../asset/iconRainfallNHDC/${iconName}.png`).default,
        iconSize: [
            20, 20
        ],
        iconAnchor: [
            10, 20
        ],
        popupAnchor: [0, -22]
    });
}

export const landslideRiskIcon = (isRisk) => {
    let iconName = 'empty.png'

    if (isRisk) {
        iconName = 'landslideRisk.svg'
    }

    return new Leaflet.Icon({
        iconUrl: require(`../asset/${iconName}`).default,
        iconSize: [
            30, 30
        ],
        iconAnchor: [
            15, 50
        ],
        popupAnchor: [0, -22]
    });
}


export const rainIcon = (value) => {
    let iconName = 'no_data'
    if (value <= 0) {
        iconName = 'no_rain'
    } else if (value > 0 && value <= 10) {
        iconName = 'rain_blue'
    } else if (value > 10 && value <= 35) {
        iconName = 'rain_yellow'
    } else if (value > 35 && value <= 90) {
        iconName = 'rain_orange'
    } else if (value > 90) {
        iconName = 'rain_red'
    }
    return new Leaflet.Icon({
        iconUrl: require(`../asset/${iconName}.svg`).default,
        iconSize: [
            20, 20
        ],
        iconAnchor: [
            10, 20
        ],
        popupAnchor: [0, -22]
    });
}


export const beacon_icons = {
    Discharge: dischargeIcon,
    Rainfall: rainIcon,
    WaterLevel: waterLevelForecastAlarmIcon
}


export const dateRange = () => {
    const count = 80;
    let date = [];
    for (let index = 0; index < count; index++) {
        date[index] = moment()
            .add(count * -1, 'day')
            .add(index, 'day')
            .format('DD-MM-YYYY');
    }
    return date;
}

export const RAINFALL_STATION = async () => {
    const res = await axios.get('https://api.mrcmekong.org/v1/time-series/rainfall/recent')
    if (res.data) {
        const data = res
            .data
            .features
            .map(item => {
                return {
                    id: item.properties.stationId,
                    name: item.properties.stationName,
                    position: [
                        item.geometry.coordinates[1], item.geometry.coordinates[0]
                    ],
                    ...item.properties
                }
            });
        console.log('data', data);
        return data
    } else {
        return []
    }
}

export const BEACON_LIST = async () => {
    const res = await axios.get('https://landsage.app:1337/api/v1/data/beaconFilter/')
    if (res.data) {
        console.log('BEACON_LIST data', res.data)
        const { Data } = res.data
        let arr = []
        Data.forEach(element => {
            const { mainType, subTypes } = element
            subTypes.forEach(item => {
                arr.push({ name: `${mainType} - ${item}`, mainType, subType: item })
            });
        });
        return arr
    } else {
        return []
    }
}

// export const TEXTURE_LIST = async () => {
//     const res = await axios.get('https://landsage.app:1337/api/v1/data/textureFilter/')
//     if (res.data) {
//         console.log('TEXTURE_LIST data', res.data)
//         const { Data } = res.data
//         let arr = []
//         Data.forEach(element => {
//             const { mainType, subTypes } = element
//             subTypes.forEach(item => {
//                 arr.push({ name: `${mainType} - ${item}`, mainType, subType: item })
//             });
//         });
//         return arr
//     } else {
//         return []
//     }
// }

export const TEXTURE_LIST = async () => {
    const res = await (await axios.get(`${API_URL}/data/layers`)).data
    if (res.data) {
        const { data } = res
        console.log('TEXTURE_LIST data', data)
        const mapped = data.map(item => {
            const { children } = item
            if (children && children.length > 0) {
                const mappedChild = children.map(child => {
                    const { fileUrl, border, date } = child
                    child.overlay = Leaflet.imageOverlay(fileUrl, border)//.setOpacity(0.64);
                    child.date = moment(date, 'YYYY-MM-DD')
                    return child
                })
                item.children = mappedChild
            }
            return item
        })
        return mapped
    } else {
        return []
    }
}

export const STATION_LIST = async () => {
    const res = await (await axios.get(`${API_URL}/data/stations`)).data
    if (res.data) {
        const { data } = res
        console.log('STATION_LIST data', data)
        const mapped = data.map(item => {
            const { typeName } = item
            item.icon = beacon_icons[typeName]
            return item
        })
        return mapped
    } else {
        return []
    }
}



export const getBeaconData = async (mainType, subTypes, dateFiltered) => {
    const res = await axios.get(`https://landsage.app:1337/api/v1/data/beaconFilter/${mainType}/${subTypes}`)
    if (res.data) {
        console.log('getBeaconData data', res.data);
        const promises = res
            .data
            .map(async item => {
                const { fileList } = item
                const { custom } = fileList[0]
                let alarmLevel = 0.0
                let floodLevel = 0.0
                const customData = {}
                if (subTypes === 'WaterLevel') {
                    alarmLevel = parseFloat(custom.find(item => item.key === 'AlarmLevel').value)
                    floodLevel = parseFloat(custom.find(item => item.key === 'FloodLevel').value)
                    customData.alarmLevel = alarmLevel
                    customData.floodLevel = floodLevel
                }

                const data = await axios.get(`https://landsage.app:1337/api/v1/data/csv/${fileList[0].fileName}`)
                const checkDate = subTypes === 'Rainfall' ? dateFiltered.format('MM/DD/YYYY').replace(/\b0/g, '') : dateFiltered.format('YYYY-MM-DD')
                let detailKeys = subTypes === 'WaterLevel' ? ['date', 'value', 'alarmLevel', 'floodLevel'] : ['date', 'value']
                if (data.data) {
                    const stationValue = data.data.data.find(item => item.Date === checkDate) || data.data.data[data.data.data.length - 1]
                    let iconValue = parseFloat(stationValue[subTypes])
                    let fill = '#666'
                    if (subTypes === 'WaterLevel') {
                        if (iconValue < customData.alarmLevel) {
                            fill = '#0f0'
                        } else if (iconValue >= customData.alarmLevel && iconValue < customData.floodLevel) {
                            fill = '#fa0'
                        } else if (iconValue >= customData.floodLevel) {
                            fill = '#f30'
                        }
                    }

                    return {
                        id: item._id,
                        country: item.country,
                        name: item.name,
                        key: `${mainType} - ${subTypes}`,
                        subTypes,
                        path: subTypes,
                        data: { ...item, date: stationValue.Date, value: iconValue, ...customData },
                        detailKeys,
                        icon: beacon_icons[subTypes](subTypes === 'WaterLevel' ? fill : iconValue),
                        timeSeriesData: data.data.data,
                        position: [
                            item.lat, item.long
                        ],
                        ...item.properties
                    }
                }
            });
        const proc = await Promise.all(promises)
        // console.log("************************proc = ", proc)
        return proc
    } else {
        return []
    }
}

export const getTextureData = async (mainType, subTypes) => {
    const res = await axios.get(`https://landsage.app:1337/api/v1/texture/`)
    console.log('getTextureData data', res.data);
    if (!res.data.err) {
        const data = []
        res
            .data
            .forEach(item => {
                const { fileList, textureName, country, mainType, subType } = item
                fileList.forEach(element => {
                    const {
                        _id,
                        border,
                        date,
                        month,
                        year,
                        legendFilename,
                        textureFilename
                    } = element
                    const mapped = {
                        id: _id,
                        mainType,
                        subType,
                        country,
                        border: [
                            [
                                border.S, border.W
                            ],
                            [border.N, border.E]
                        ],
                        textureUrl: `https://landsage.app:1337/api/v1/data/texture/${textureFilename}`,
                        legendUrl: `https://landsage.app:1337/api/v1/data/texture/${legendFilename}`,
                        name: `${mainType} - ${subType}`,
                        date: moment(`${year}-${month}-${date}`),
                    }
                    mapped.overlay = Leaflet.imageOverlay(mapped.textureUrl, mapped.border).setOpacity(0.64);
                    data.push(mapped);
                });
            });

        return data
    } else {
        return []
    }
}

export const WATER_LEVEL = async () => {
    const res = await axios.get('https://landsage.app:1337/api/v1/data/beacon')
    if (res.data) {
        const data = res.data
        const wt = data.filter(item => {
            return item
                .fileList
                .some(obj => obj.mainType === 'Flooding') && item
                    .fileList
                    .some(obj => obj.subType === 'WaterLevel')
        })
        const mapped = wt.map(item => {
            return {
                id: item._id,
                name: item.name,
                position: [
                    item.lat, item.long
                ],
                ...item
            }
        });
        console.log('mapped', mapped);
        return mapped
    } else {
        return []
    }
}

export const WATER_LEVEL_NationalHydroinformaticsDataCenter = async () => {
    const res = await axios.get('https://api-v3.thaiwater.net/api/v1/thaiwater30/public/waterlevel_load');
    if (res.data && res.data.waterlevel_data && res.data.waterlevel_data.data) {
       //console.log('*****************', res.data);

        const data = res.data.waterlevel_data.data.filter(function(item) {
            if(!item.station.tele_station_lat) return false;
            if(!item.station.tele_station_long) return false;
            if(item.waterlevel_msl <= 0) return false;

            return true;
          }).map(function(item) {

            let situation = "";
            if(item.storage_percent <= 10){
                situation = "น้อยวิกฤต";
            }else if(item.storage_percent > 10 && item.storage_percent <= 30){
                situation = "น้อย";
            }else if(item.storage_percent > 30 && item.storage_percent <= 70){
                situation = "ปกติ";
            }else if(item.storage_percent > 70 && item.storage_percent <= 100){
                situation = "มาก";
            }else if(item.storage_percent > 100){
                situation = "ล้นตลิ่ง";
            }

            if(item.waterlevel_msl > item.waterlevel_msl_previous){
                situation = situation + ' (เพิ่มขึ้น ' + (item.waterlevel_msl - item.waterlevel_msl_previous).toFixed(2) + ' ม.รทก.)';
            }else if(item.waterlevel_msl < item.waterlevel_msl_previous){
                situation = situation + ' (ลดลง ' + (item.waterlevel_msl_previous - item.waterlevel_msl).toFixed(2) + ' ม.รทก.)';
            }

            return {
                country: "TH",
                id: item.station.id + "",
                stationId: item.station.id + "",
                name: item.station.tele_station_name.en || item.station.tele_station_name.th || "undefined",
                position: [
                    item.station.tele_station_lat, item.station.tele_station_long
                ],
                lat: item.station.tele_station_lat,
                long: item.station.tele_station_long,
                updatedAt: item.waterlevel_datetime,
                createdAt: item.waterlevel_datetime,
                'Percent': item.storage_percent,
                'Situation': situation,
                Timestamp: item.waterlevel_datetime,
                'Water Level': item.waterlevel_msl,
                WaterLevelPrev: item.waterlevel_msl_previous,
                'Ground Level': item.station.ground_level
            }
        });
        return data;
    }else{
        return [];
    }
}
export const RAINFALL_NationalHydroinformaticsDataCenter_STATION = async () => {
    const res = await axios.get('https://api-v3.thaiwater.net/api/v1/thaiwater30/public/rain_24h');
    if (res.data && res.data.data) {

        let isDebugRiskLandslide = sessionStorage.getItem("isDebugRiskLandslide") === 'true' ? true: false;
        // let latBottomLeftLandslide = 18.766996
        // let lonBottomLeftLandslide = 99.636653
        // let latTopRightLandslide = 19.778016
        // let lonTopRightLandslide = 100.676356

        const data = res.data.data.filter(function(item) {
            if (item.rain_24h === 0) {
              return false; //continue;
            }
            return true;
          }).map(function(item) {

            let rain = item.rain_24h;
            if(isDebugRiskLandslide && item.station.tele_station_lat == 19.393637 && item.station.tele_station_long == 100.45059){
                rain = 100;
            }

            return {
                stationId: item.station.id,
                id: item.station.id,
                name: item.station.tele_station_name.en || item.station.tele_station_name.th || "undefined",
                position: [
                    item.station.tele_station_lat || 0, item.station.tele_station_long || 0
                ],
                Rain: rain,
                Timestamp: item.rainfall_datetime,
                ...item
            }
        });


        return data;
    }else{
        return [];
    }
}

export const RainfallTMD_STATION = async () => {
    try {
        const res = await(await axios.get('https://landsage.app:1337/api/v1/data/rainfallTMD')).data;
        return res.data;
    } catch (error) {
        console.log('error :>> ', error.message);
        return []
    }

    // const res = await axios.get('https://data.tmd.go.th/api/Weather3Hours/V2/?uid=api&ukey=api12345');

    // if (res.data) {
    //     var parser = new DOMParser();
    //     var xmlDoc = parser.parseFromString(res.data, "text/xml");
    //     var stations = xmlDoc.querySelectorAll("Station");

    //     // Loop through each station element
    //     let stationTMDvalue = [];
    //     for (var i = 0; i < stations.length; i++) {
    //         // Extract the values from the latitude and longitude elements
    //         var stationNameThai = stations[i].querySelector("StationNameThai").textContent;
    //         var stationNameEnglish = stations[i].querySelector("StationNameEnglish").textContent;
    //         var latitudeValue = stations[i].querySelector("Latitude").textContent;
    //         var longitudeValue = stations[i].querySelector("Longitude").textContent;

    //         var observation = stations[i].querySelector("Observation");
    //         var dateTime = observation.querySelector("DateTime").textContent;
    //         var rainfall24Hr = observation.querySelector("Rainfall24Hr").textContent;

    //         stationTMDvalue.push({
    //             stationNameThai: stationNameThai,
    //             stationNameEnglish: stationNameEnglish,
    //             latitudeValue: latitudeValue,
    //             longitudeValue: longitudeValue,
    //             dateTime: dateTime,
    //             rainfall24Hr: rainfall24Hr
    //         })

    //         // console.log("stationNameThai: " + stationNameThai);
    //         // console.log("stationNameEnglish: " + stationNameEnglish);
    //         // console.log("Latitude: " + latitudeValue);
    //         // console.log("Longitude: " + longitudeValue);
    //         // console.log("DateTime: " + dateTime);
    //         // console.log("Rainfall24Hr: " + rainfall24Hr);
    //         // console.log("________________");
    //     }
    
    //     const data = stationTMDvalue.filter(function(item) {
    //         return true;
    //       }).map(function(item) {
    //         return {
    //             name: item.stationNameEnglish || item.stationNameThai || "undefined",
    //             position: [
    //                 item.latitudeValue, item.longitudeValue
    //             ],
    //             Rain: item.rainfall24Hr,
    //             Timestamp: item.dateTime,
    //             ...item
    //         }
    //     });

    //     return data;
    // }else{
    //     return [];
    // }
}


export const NRTH_MONITOR = async () => {
    const res = await axios.get('https://landsage.app:1337/api/v1/data/nrth')
    if (res.data) {
        console.log('res.data', res.data)
        return res.data
    } else {
        return []
    }
}
export const UbonLandUseImageOverlay = Leaflet.imageOverlay(ubonLandusePic, [
    [
        14.207903, 104.370504
    ],
    [16.096864, 105.637333]
]).setOpacity(0.5);
export const UbonPopulationImageOverlay = Leaflet.imageOverlay(ubonPopulationPic, [
    [
        14.204734, 104.366532
    ],
    [16.102849, 105.645559]
]).setOpacity(1.0);
export const MRC_BOUND_GEOJSON = Leaflet.geoJSON(mrcbound, {
    style: function (feature) {
        console.log('feature', feature);
        return { color: feature.properties.stroke };
    }
});
export const MRC_MEKONG_RIVER_GEOJSON = Leaflet.geoJSON(mrcmekong, {});

export const DATA_LAYER_LIST = [
    // {
    //     name: 'Water Level Station',
    //     type: 'marker',
    //     path: 'WaterLevel',
    //     icon: rainIcon(),
    //     function: WATER_LEVEL,
    //     detailKeys: ['name', 'id', 'fileList']
    // },
     {
        name: 'Rainfall Station',
        type: 'marker',
        path: 'RainFall',
        icon: rainIcon(),
        legendUrl: require('../asset/rainfall-legend.png'),
        function: RAINFALL_STATION,
        detailKeys: [
            'name',
            'stationId',
            'lastTimestamp',
            'last15min',
            'last24hr',
            'last7day'
        ]
    }, {
        name: 'Hydro Meteorological',
        type: 'marker',
        path: 'Hydro',
        legendUrl: require('../asset/hydro-legend.png'),
        icon: customIcon,
        function: NRTH_MONITOR,
        detailKeys: [
            'name',
            'stationId',
            'lastMeasurement',
            'waterLevel',
            'rainFall',
            'rainFall1H',
            'rainFall7to7',
            'rainFall24H'
        ]
    }, {
        name: 'MRC Bound',
        type: 'overlay',
        overlay: MRC_BOUND_GEOJSON
    }, {
        name: 'MRC MEKONG RIVER',
        type: 'overlay',
        overlay: MRC_MEKONG_RIVER_GEOJSON
    }, {
        name: 'Land used',
        type: 'overlay',
        overlay: UbonLandUseImageOverlay
    }, {
        name: 'Population',
        type: 'overlay',
        overlay: UbonPopulationImageOverlay
    }, {
        name: 'Water Level Station Of National Hydroinformatics Data Center',
        type: 'marker',
        path: 'WaterLevelNHDC',
        icon: function(percent, water, waterPrev){
            return waterNHDCIcon(percent, water, waterPrev);
        },
        legendUrl: require('../asset/waterLevelNHDC-legend.png'),
        function: WATER_LEVEL_NationalHydroinformaticsDataCenter,
        detailKeys: ['Water Level', 'Percent', 'Situation', 'Timestamp', 'Ground Level']
    }, {
        name: 'Rainfall Station Of National Hydroinformatics Data Center',
        type: 'marker',
        path: 'RainFallNHDC',
        icon: function(rain){
            return rainNHDCIcon(rain);
        },
        legendUrl: require('../asset/RainfallNHDC-legend.png'),
        function: RAINFALL_NationalHydroinformaticsDataCenter_STATION,
        detailKeys: [
            'Rain',
            'Timestamp'
        ]
    }, {
        name: 'RainfallTMD',
        type: 'marker',
        path: 'RainFallTMD',
        icon: function(rain){
            return rainNHDCIcon(rain);
        },
        legendUrl: require('../asset/RainfallNHDC-legend.png'),
        function: RainfallTMD_STATION,
        detailKeys: [
            'Rain',
            'Timestamp'
        ]
    }, {
        name: 'Landslide Assessment',
        type: 'marker',
        path: 'LandslideAssessment',
        icon: function(isRisk){
            return landslideRiskIcon(isRisk);
        },
        legendUrl: require('../asset/RainfallNHDC-legend.png'),
        function: requestLandslideRiskFromServer,
        detailKeys: [
            'rain_level',
            'isRisk'
        ]
    }

]

/* View in fullscreen */
export function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {/* Safari */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {/* IE11 */
        elem.msRequestFullscreen();
    }
}

/* Close fullscreen */
export function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {/* Safari */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {/* IE11 */
        document.msExitFullscreen();
    }
}
export const COUNTRY_LISTS = async () => {
    const res = await axios.get(`${API_URL}/data/location`);
    if (res.data) {
        //console.log('Country Data:', res.data)
        return res.data
    } else {
        return []
    }
}