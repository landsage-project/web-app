import React, { Component } from 'react'
import Leaflet from 'leaflet';
import {
    MapContainer,
    TileLayer,
    LayersControl,
} from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import io from 'socket.io-client'
import {
    Button,
    Dropdown,
    DropdownButton
} from 'react-bootstrap';
import moment from 'moment';
import {
    Spin, Input, Drawer, Slider, Select, TreeSelect, Checkbox, message, Row,
    Col, Button as AntdButton, Modal
} from 'antd';
import axios from 'axios';
import { DataMarker } from './components/DataMarker'
import { DataMarkerNHDC } from './components/DataMarkerNHDC'
import { BeaconMarker } from './components/BeaconMarker'
import { PulseIcon } from './components/PulseMarker'
import GooglePlacesAutocomplete, { geocodeByPlaceId } from 'react-google-places-autocomplete';
import Draggable from 'react-draggable';
import { DragDropContext, Droppable, Draggable as DraggableItem } from "react-beautiful-dnd";

import './App.css'
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-slider/dist/css/bootstrap-slider.css"
import 'bootstrap/dist/css/bootstrap.min.css';

import {
    dateRange,
    DATA_LAYER_LIST,
    openFullscreen,
    closeFullscreen,
    BEACON_LIST,
    TEXTURE_LIST,
    getBeaconData,
    getTextureData,
    COUNTRY_LISTS,
    STATION_LIST
} from './helper'

const VERSION = '2.2.2'

const { Option } = Select

const satelliteUrl = `https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}`
const normalUrl = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`

//const socket = io('http://167.71.203.190:8080')
const socket = io('https://landsage.app:8080')
const EVENT_LIST = [
    'Location',
    'Global Sync',
    'Station Data',
    'Texture Layer',
    'Country',
    'Date Slider',
    'Sort Texture',
    'Pulse Marker'
]
let CHUNK = {}
const dateRangeDummy = dateRange();

const SITE = ['GLOBAL', 'THAILAND', 'VIETNAM', 'LAOS', 'CAMBODIA']

const legends_beacon = {
    Rainfall: require('./asset/Rainfall_Legend.png').default,
    WaterLevel: require('./asset/WaterLevel_Legend.png').default,
    Discharge: require('./asset/Discharge_Legend.png').default
}
const API_URL = 'https://landsage.app:1337/api/v1'
export default class MapDisplay extends Component {
    state = {
        lat: 15.2447,
        lng: 104.8475,
        zoom: 7,
        isSync: true,
        currentSite: 'GLOBAL',
        currentDateRangeValue: dateRangeDummy.length - 1,
        currentDateRange: dateRangeDummy[dateRangeDummy.length - 1],
        availableDataLayerKey: {},
        availableBeaconLayerKey: {},
        availableCountryFilterKey: {
            TH: true,
            VN: true,
            LA: true,
            KH: true,
            MRB: true
        },
        availableLegendKey: {},
        availableClipKey: {},
        hideLegendKey: {},
        hideClipKey: {},
        dataLayerList: {},
        beaconLayerList: {},
        unSyncKey: {},
        isShowDataLayerPanel: false,
        isShowCountryFilterPanel: false,
        isShowSyncProfilePanel: false,
        isShowBeaconPanel: false,
        isShowTexturePanel: false,
        isFullScreen: false,
        isShowBottomDrawer: false,
        isShowSettingDrawer: true,
        isShowLayerDrawer: false,
        isShowbookmarkDrawer: false,
        activeDrags: false,
        dateFiltered: moment(),
        beaconLists: [],
        beaconLayers: {},
        textureList: [],
        textureLayers: {},
        overlays: [],
        baseMap: normalUrl,
        locationTree: [],
        filterTree: [],
        selectedContries: [],
        selectedAreas: [],
        selectedDataTypes_texture: [],
        selectedDataTypes_station: [],
        sortedTextureItems: [],
        lastProcessStationList: [],
        selfZoom: false,
        stationFinishLoading: false,
        textureFinishLoading: false,
        pulseMarkerMode: false,
        siteChannel: [],
        bookmarkLocations: []
    }
    mapRef = React.createRef()
    markerRefs = []

    componentDidMount = async () => {
        this.initialSocketEvent();
        const locationTree = await COUNTRY_LISTS();
        this.setState({ locationTree: locationTree.data })
        const filterTree = await (await axios(`${API_URL}/data/dataTypes`)).data;
        this.setState({ filterTree: filterTree.data })
        const beaconLists = await STATION_LIST();
        this.setState({ beaconLists, stationFinishLoading: true })
        const textureList = await TEXTURE_LIST();
        this.setState({ textureList, textureFinishLoading: true });
        const bookmarkLocations = localStorage.getItem('bookmarkLocations') ? JSON.parse(localStorage.getItem('bookmarkLocations')) : [];
        this.setState({ bookmarkLocations })
        this.initialIntervalUpdate();

    }

    initialIntervalUpdate() {
        setInterval(() => {
            console.log("update beacon_2", this.state);
            this.updateBeacon_2();
        }, 1800000);
        //1800000 = 30 minutes
    }

    initialSocketEvent() {
        const { unSyncKey } = this.state
        console.log('unSyncKey', unSyncKey);

        socket.on("connect", () => {
            console.log(socket.id);
            socket.emit('JOIN', this.state.currentSite);
        });
        socket.on('SEND_MAP_CAMERA', data => {
            if (unSyncKey['Location'] || !this.state.isSync)
                return;
            //console.log('SEND_MAP_CAMERA', data)
            const center = {
                lat: data.lat,
                lng: data.lng
            }
            this.setState({ lat: data.lat, lng: data.lng })
            if (this.state.isSync) {
                this
                    .mapRef
                    .current
                    .setView(center, this.state.zoom)
            }

        })
        socket.on('SEND_MAP_ZOOM', data => {
            if (unSyncKey['Location'] || !this.state.isSync)
                return;
            console.log('SEND_MAP_ZOOM', data)
            this.setState({ zoom: data.zoom, lat: data.center.lat, lng: data.center.lng })
            if (this.state.isSync) {
                this.mapRef.current.setView(data.center, data.zoom)
                this.setState({ selfZoom: true })
            }
        })
        socket.on('SEND_SYNC', data => {
            if (unSyncKey['Global Sync'])
                return;
            this.setState({ isSync: data })
        })
        socket.on('SEND_CHANGE_DATE_RANGE', data => {
            if (unSyncKey['Date Slider'] || !this.state.isSync)
                return;
            const { dateFiltered } = this.state;
            const year = dateFiltered.year();
            const newYear = moment(data).year();
            console.log('SEND_CHANGE_DATE_RANGE', data);
            this.setState({
                dateFiltered: moment(data)
            }, () => {
                if (year != newYear) {
                    this.updateTextureOverlay_2();
                }
                this.updateBeacon_2();
            })
        })
        socket.on('SEND_SELECT_DATA_LAYER', async data => {
            if (unSyncKey['Station Data'] || !this.state.isSync)
                return;
            const { availableDataLayerKey, dataLayerList } = this.state
            const item = DATA_LAYER_LIST.find(obj => obj.name === data.name);
            if (data && data.value) {
                availableDataLayerKey[data.name] = data.value
                this.setState({ availableDataLayerKey })
                if (item.type === 'overlay') {
                    this.toggleImageOverlay(item);
                } else {
                    const dataRes = await item.function();
                    dataLayerList[data.name] = dataRes
                    this.setState({ dataLayerList })
                }
            } else {
                if (item.type === 'overlay') {
                    this.toggleImageOverlay(item);
                }
                delete dataLayerList[data.name]
                delete availableDataLayerKey[data.name]
                this.setState({ dataLayerList, availableDataLayerKey })
            }
        })
        socket.on('SEND_BEACON_SELECTED', async data => {
            if (unSyncKey['Station Data'] || !this.state.isSync)
                return;
            if (data) {
                console.log('SEND_BEACON_SELECTED', data);
                this.setState({
                    selectedDataTypes_station: data
                }, async () => {
                    this.updateBeacon_2();
                })
            }
        })
        socket.on('SEND_TEXTURE_SELECTED', data => {
            if (unSyncKey['Texture Layer'] || !this.state.isSync)
                return;
            if (data) {
                console.log('SEND_TEXTURE_SELECTED', data);
                this.setState({
                    selectedDataTypes_texture: data
                }, () => {
                    this.updateTextureOverlay_2();
                });
            }
        })
        socket.on('SEND_COUNTRY_SELECTED', data => {
            if (unSyncKey['Country'] || !this.state.isSync)
                return;
            const { selectedContries, selectedAreas } = data;
            if (selectedContries && selectedAreas) {
                this.setState({ selectedContries, selectedAreas }, () => {
                    this.updateTextureOverlay_2();
                    this.updateBeacon_2();
                });
            }
        })
        socket.on('SEND_SORTED_TEXTURE', data => {
            if (unSyncKey['Sort Texture'] || !this.state.isSync)
                return;
            if (data) {
                console.log('SEND_SORTED_TEXTURE', data);
                this.setState({ sortedTextureItems: data }, () => {
                    this.updateTextureOverlay_2();
                });
            }
        })
        socket.on('SEND_PULSE_MARKER', data => {
            if (unSyncKey['Pulse Marker'] || !this.state.isSync)
                return;
            if (data) {
                console.log('SEND_PULSE_MARKER', data);
                this.pulsing({ latlng: data })

            }
        })
        socket.on('rainfall_nrt', data => {
            const { availableDataLayerKey, dataLayerList } = this.state
            if (unSyncKey['Station Data'] || !availableDataLayerKey['Rainfall Station'] || !this.state.isSync)
                return;

            dataLayerList['Rainfall Station'] = data
            this.setState({ dataLayerList })
        })
        socket.on('hydro_nrt', data => {
            const { availableDataLayerKey, dataLayerList } = this.state
            if (unSyncKey['Station Data'] || !availableDataLayerKey['Hydro Meteorological'] || !this.state.isSync)
                return;
            dataLayerList['Hydro Meteorological'] = data
            this.setState({ dataLayerList })
        })
    }
    toggleImageOverlay = (data) => {
        const { overlay } = data
        const map = this.mapRef.current;
        if (map.hasLayer(overlay)) {
            overlay.removeFrom(map);
        } else {
            overlay.addTo(map);
        }
    }
    updateTextureOverlay_2 = async () => {
        const { selectedDataTypes_texture, textureList, dateFiltered, selectedContries, selectedAreas, sortedTextureItems, availableLegendKey, availableClipKey } = this.state
        const map = this.mapRef.current;
        console.log('selectedDataTypes_texture', selectedDataTypes_texture);
        console.log('textureList', textureList);
        textureList.forEach(item => {
            item.children.forEach(child => {
                child.overlay.removeFrom(map);
                delete availableLegendKey[child.objectId]
                delete availableClipKey[child.objectId]
            })
        })
        console.log('dateFiltered', dateFiltered);
        let filteredData = [...textureList].filter(item => selectedDataTypes_texture.includes(item.typeId))
        console.log('1 filteredData', filteredData);
        filteredData = filteredData.filter(item => selectedContries.some(r => item.countryId.includes(r)) || selectedAreas.some(r => item.areaId.includes(r)))
        filteredData = filteredData.map(item => {
            const { children } = item
            const sorted = children.sort((a, b) => {
                const diff_a = a.date.diff(dateFiltered)
                const diff_b = b.date.diff(dateFiltered)
                return Math.abs(diff_a) - Math.abs(diff_b)
            })
            item.children = sorted
            return item
        })
        console.log('filteredData', filteredData);
        const newSort = sortedTextureItems.filter(item => filteredData.map(item => item.objectId).includes(item))
        console.log('newSort', newSort);
        const notInSort = filteredData.filter(item => !newSort.includes(item.objectId))
        console.log('notInSort', notInSort);

        filteredData = [...notInSort, ...newSort.map(item => filteredData.find(ch => ch.objectId === item))]
        this.setState({ sortedTextureItems: filteredData.map(item => item.objectId) })
        console.log('final filteredData', filteredData);
        filteredData.reverse().forEach(item => {
            item.children[0].overlay.addTo(map);
            //availableLegendKey[item.children[0].objectId] = true
        });
        this.setState({ availableLegendKey, availableClipKey })
    }

    updateBeacon_2 = async () => {
        const { selectedDataTypes_station, beaconLists, dateFiltered, selectedContries, selectedAreas, availableLegendKey } = this.state;
        beaconLists.forEach(item => {
            delete availableLegendKey[item.typeName]
        })
        let filteredData = [...beaconLists].filter(item => selectedDataTypes_station.includes(item.typeId))
        console.log('selectedContries station', selectedContries);
        console.log('selectedAreas station', selectedAreas);
        console.log('before filteredData station', filteredData);
        filteredData = filteredData.filter(item => selectedContries.some(r => item.countryId.includes(r)) || selectedAreas.some(r => item.areaId.includes(r)))
        console.log('filteredData station', filteredData);
        const promises = filteredData.map(async item => {
            const { stationCode, typeName, objectId } = item
            // let tempData = localStorage.getItem(objectId) && JSON.parse(localStorage.getItem(objectId));
            // if (!tempData) {
            //     const csvData = await (await axios(`${API_URL}/data/csvData/${objectId}`)).data
            //     localStorage.setItem(objectId, JSON.stringify(csvData.data));
            //     tempData = csvData.data;
            // }
            //const csvData = await (await axios(`${API_URL}/data/csvData/${objectId}`)).data

            let tempData = CHUNK[objectId]
            if (!tempData) {
                const csvData = await (await axios(`${API_URL}/data/csvData/${objectId}`)).data
                CHUNK[objectId] = csvData.data
                tempData = csvData.data;
            }
            const hydroData = await (await axios(`${API_URL}/data/hydroData?stationCode=${stationCode}&typeName=${typeName}`)).data
            item.timeSeriesData = [...tempData, ...hydroData.data]
            return item
        })

        filteredData = await Promise.all(promises)
        document.querySelector(".leaflet-container").style.cursor = "wait";
        const promisesFilteredData = filteredData.map(async item => {
            const { timeSeriesData, stationCode, typeName, metaData, icon, objectId } = item
            const dateStr = dateFiltered.format('YYYY-MM-DD')
            const directData = timeSeriesData.find(item => item.date === dateStr)
            if (directData) {
                item.sortedTimeSeriesData = directData
            } else {
                const sorted = timeSeriesData.sort((a, b) => {
                    const diff_a = moment(a.date, 'YYYY-MM-DD').diff(dateFiltered)
                    const diff_b = moment(b.date, 'YYYY-MM-DD').diff(dateFiltered)
                    return Math.abs(diff_a) - Math.abs(diff_b)
                })
                console.log('sorted[0]', sorted[0]);
                item.sortedTimeSeriesData = sorted[0]
            }

            let fill = '#666'
            let floodLevelVal = 0;
            let alarmLevelVal = 0;
            const iconValue = parseFloat(item.sortedTimeSeriesData.value, 0.0)
            if (typeName === 'WaterLevel') {
                const AlarmLevel = metaData && metaData.find(item => item.Key === 'AlarmLevel')?.Value
                const FloodLevel = metaData && metaData.find(item => item.Key === 'FloodLevel')?.Value
                console.log('AlarmLevel', AlarmLevel);
                console.log('FloodLevel', FloodLevel);

                if (iconValue < parseFloat(AlarmLevel, 0.0)) {
                    fill = '#0f0'
                } else if (iconValue >= parseFloat(AlarmLevel, 0.0) && iconValue < parseFloat(FloodLevel, 0.0)) {
                    fill = '#fa0'
                } else if (iconValue >= parseFloat(FloodLevel, 0.0)) {
                    fill = '#f30'
                }
                floodLevelVal = parseFloat(FloodLevel, 0.0);
                alarmLevelVal = parseFloat(AlarmLevel, 0.0);
            }

            item.urlDataHistorical = `${API_URL}/data/csvData/${objectId}`;
            // item.urlData = `${API_URL}/data/hydroData?stationCode=${stationCode}&typeName=${typeName}`;
            if(typeName === 'WaterLevel'){
                item.renderIcon = await icon(fill, floodLevelVal, alarmLevelVal, `${API_URL}/data/csvData/${objectId}`, `${API_URL}/data/hydroData?stationCode=${stationCode}&typeName=${typeName}`);
                // item.renderIcon = icon(fill);
            }else{
                item.renderIcon = icon(iconValue)
            }

            // item.renderIcon = icon(typeName === 'WaterLevel' ? fill : iconValue)
            availableLegendKey[typeName] = true

            return item
        })

        filteredData = await Promise.all(promisesFilteredData)
        document.querySelector(".leaflet-container").style.cursor = "";
        console.log('filteredData station', filteredData);
        this.setState({ lastProcessStationList: filteredData, availableLegendKey })
    }

    updateTextureOverlay = async () => {
        const { availableDataLayerKey, dateFiltered, availableCountryFilterKey } = this.state;
        let { overlays } = this.state;
        const res = await getTextureData();
        const map = this.mapRef.current;
        overlays.forEach(overlay => {
            overlay.removeFrom(map);
        });
        overlays = [];
        const data = {};
        let filtered = res.filter(element => availableDataLayerKey[element.name]);
        filtered = filtered.filter(element => availableCountryFilterKey[element.country]);
        filtered.forEach(item => {
            if (!data[item.name]) {
                data[item.name] = {};
            }
            if (!data[item.name][item.country]) {
                data[item.name][item.country] = item;
            } else {
                const temp = data[item.name][item.country];
                const tempDiff = Math.abs(dateFiltered.diff(temp.date));
                const diff = Math.abs(dateFiltered.diff(item.date));
                console.log(`tempDiff : ${tempDiff} diff:${diff}`)
                if (diff <= tempDiff) {
                    data[item.name][item.country] = item;
                }
            }

        });
        console.log('data', data);
        Object
            .keys(data)
            .forEach(key => {
                Object
                    .keys(data[key])
                    .forEach(keyCountry => {
                        const elem = data[key][keyCountry];
                        const { overlay } = elem;
                        overlay.data = elem;
                        overlay.addTo(map);
                        overlays.push(overlay);
                    });
            });

        this.setState({
            overlays
        }, () => {
            console.log('overlays', overlays)
        });
    }

    updateBeacon = async () => {
        const { availableBeaconLayerKey, dateFiltered, availableCountryFilterKey } = this.state;
        let { beaconLayerList } = this.state;
        Object
            .keys(availableBeaconLayerKey)
            .forEach(async key => {
                const mainType = key.split(' - ')[0]
                const subType = key.split(' - ')[1]
                const res = await getBeaconData(mainType, subType, dateFiltered);
                const filtered = res.filter(element => availableCountryFilterKey[element.country]);
                beaconLayerList[key] = filtered;
                console.log('beaconLayerList', beaconLayerList);

                this.setState({ beaconLayerList });
            });
    }

    handleClickMarker = (event, id) => {
        const { lat, lng } = event.latlng
        console.log(`click id`, id);
        const center = {
            lat: lat,
            lng: lng
        }
        setTimeout(() => {
            this
                .mapRef
                .current
                .setView(center, this.state.zoom);
            if (this.state.unSyncKey['Location'] || !this.state.isSync)
                return;
            socket.emit('MAP_CAMERA', center)
        }, 200);

    }
    onChangeSite = async (data) => {
        this.setState({ currentSite: data })
        socket.emit('JOIN', data);

    }
    handleSlider = async (e) => {
        console.log(e.target.value);
        this.setState({
            currentDateRange: dateRangeDummy[e.target.value],
            currentDateRangeValue: e.target.value
        })
        if (this.state.unSyncKey['Date Slider'] || !this.state.isSync)
            return;
        socket.emit('CHANGE_DATE_RANGE', e.target.value);
    }

    renderDataLayerPanel() {
        const { dataLayerList, availableDataLayerKey, availableLegendKey } = this.state
        return (
            <div className="leaflet-control-container">
                <div>
                    <div style={{ marginTop: 12, marginBottom: 8 }}>{'Select External Data: '}</div>
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="Please select"
                        onSelect={async (value) => {
                            console.log('select', value);
                            availableDataLayerKey[value] = true;
                            availableLegendKey[value] = true;
                            this.setState({ availableDataLayerKey, availableLegendKey });
                            const item = DATA_LAYER_LIST.find(item => item.name === value);
                            if (item.type === 'overlay') {
                                this.toggleImageOverlay(item);
                            } else {
                                const data = await item.function();
                                dataLayerList[item.name] = data;
                                if(item.name ===  "Rainfall Station Of National Hydroinformatics Data Center"){
                                    const landlsideItem = DATA_LAYER_LIST.find(item => item.name === "Landslide Assessment");

                                    setInterval(async () => {
                                        console.log("**update Rainfall Station Of National Hydroinformatics Data Center");

                                        const data = await item.function();
                                        dataLayerList[item.name] = data;

                                        const dataLandlside = await landlsideItem.function(dataLayerList[item.name]);
                                        dataLayerList[landlsideItem.name] = dataLandlside;
                                        this.setState({ dataLayerList });
                                    }, 1800000);
                                    // 1800000 = 30 minutes

                                    const dataLandlside = await landlsideItem.function(dataLayerList[item.name]);
                                    dataLayerList[landlsideItem.name] = dataLandlside;
                                }
                                // console.log("item.name = ", item.name, " dataLayerList = ", dataLayerList);
                                this.setState({ dataLayerList });
                            }
                            if (this.state.unSyncKey['Station Data'] || !this.state.isSync)
                                return;
                            socket.emit('SELECT_DATA_LAYER', {
                                name: item.name,
                                value: true
                            })
                        }}
                        onDeselect={async (value) => {
                            console.log('de seelect', value);
                            const item = DATA_LAYER_LIST.find(item => item.name === value);

                            if (item.type === 'overlay') {
                                this.toggleImageOverlay(item);
                            } else {
                                delete dataLayerList[item.name];
                                this.setState({ dataLayerList })
                            }
                            delete availableDataLayerKey[item.name];
                            delete availableLegendKey[item.name];
                            this.setState({ availableDataLayerKey, availableLegendKey });
                            if (this.state.unSyncKey['Station Data'] || !this.state.isSync)
                                return;
                            socket.emit('SELECT_DATA_LAYER', {
                                name: item.name,
                                value: false
                            })

                        }}
                    >
                        {DATA_LAYER_LIST.map(item => {
                            if(item.name === "Landslide Assessment") return;
                            return (<Option value={item.name}>{item.name}</Option>);
                        })}
                    </Select>
                </div>
            </div>
        )
    }


    renderBeaconPanel() {
        const { filterTree, stationFinishLoading } = this.state
        const tree = filterTree.filter(item => item.name === "Flooding")[0]?.children
        return (
            <div className="leaflet-control-container">
                <div>
                    <div style={{ marginTop: 12, marginBottom: 8 }}>{'Select Data Types for Stations: '}</div>
                    {tree && stationFinishLoading ? (
                        <TreeSelect
                            showSearch
                            style={{
                                width: '100%',
                            }}
                            dropdownStyle={{
                                maxHeight: 400,
                                overflow: 'auto',
                            }}
                            treeData={tree}
                            placeholder="Please select"
                            allowClear
                            multiple
                            treeDefaultExpandAll
                            onChange={(value) => {
                                console.log('tree onChange value', value);
                                this.setState({ selectedDataTypes_station: value }, () => {
                                    this.updateBeacon_2()
                                    if (this.state.unSyncKey['Station Data'] || !this.state.isSync)
                                        return;
                                    socket.emit('BEACON_SELECTED', value);
                                })
                            }}
                        />
                    ) : <Spin />}
                </div>
            </div>
        )
    }

    renderTexturePanel() {
        const {
            filterTree,
            textureFinishLoading
        } = this.state
        const tree = filterTree.filter(item => item.name === "Landslide")[0]?.children
        return (
            <div className="leaflet-control-container">
                <div>
                    <div style={{ marginTop: 12, marginBottom: 8 }}>{'Select Data Types for Textures: '}</div>
                    {tree && textureFinishLoading ? (
                        <TreeSelect
                            showSearch
                            style={{
                                width: '100%',
                            }}
                            dropdownStyle={{
                                maxHeight: 400,
                                overflow: 'auto',
                            }}
                            treeData={tree}
                            placeholder="Please select"
                            allowClear
                            multiple
                            treeDefaultExpandAll
                            onChange={(value) => {
                                console.log('tree onChange value', value);
                                this.setState({ selectedDataTypes_texture: value }, () => {
                                    this.updateTextureOverlay_2()
                                    if (this.state.unSyncKey['Texture Layer'] || !this.state.isSync)
                                        return;
                                    socket.emit('TEXTURE_SELECTED', value);
                                })
                            }}
                        />
                    ) : <Spin />}
                </div>
            </div>
        )
    }

    renderCountryFilterPanel() {
        const { dataLayerList, availableCountryFilterKey, locationTree } = this.state
        return (
            <div className="leaflet-control-container">
                <div>
                    <div style={{ marginTop: 12, marginBottom: 8 }}>{'Select Country Filter: '}</div>
                    {locationTree && (
                        <TreeSelect
                            showSearch
                            style={{
                                width: '100%',
                            }}
                            dropdownStyle={{
                                maxHeight: 400,
                                overflow: 'auto',
                            }}
                            treeData={locationTree}
                            placeholder="Please select"
                            allowClear
                            multiple
                            treeDefaultExpandAll
                            onChange={(value) => {
                                console.log('tree onChange value', value);
                                const selectedContries = value.filter(item => item.indexOf('country') >= 0).map(item => item.replace('country-', ''))
                                const selectedAreas = value.filter(item => item.indexOf('area') >= 0).map(item => item.replace('area-', ''))
                                this.setState({ selectedContries, selectedAreas, }, () => {
                                    this.updateTextureOverlay_2();
                                    this.updateBeacon_2()
                                    if (this.state.unSyncKey['Country'] || !this.state.isSync)
                                        return;
                                    socket.emit('COUNTRY_SELECTED', { selectedContries, selectedAreas });
                                })
                            }}
                        />
                    )}

                </div>
            </div>
        )
    }

    renderUnSyncPanel() {
        const { unSyncKey } = this.state;
        return (
            <div className="leaflet-control-container">
                <div>
                    <div style={{ marginTop: 12, marginBottom: 8 }}>{'Select Sync Profile: '}</div>
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: '100%' }}
                        placeholder="Please select"
                        defaultValue={EVENT_LIST}
                        onSelect={async (value) => {
                            console.log('de seelect', value);
                            delete unSyncKey[value];
                            this.setState({ unSyncKey });
                        }}
                        onDeselect={async (value) => {
                            console.log('select', value);
                            unSyncKey[value] = true;
                            this.setState({ unSyncKey });
                        }}
                    >
                        {EVENT_LIST.map(item => (<Option value={item}>{item}</Option>))}
                    </Select>
                </div>
            </div>
        )
    }

    renderBottomDrawer() {
        const { isShowBottomDrawer, dateFiltered } = this.state
        const years = [2000, 2030]
        let year_marks = {}
        year_marks[moment().year()] = moment().year()
        for (let index = years[0]; index <= years[1]; index += 5) {
            year_marks[index] = index
        }
        let month_marks = {}
        for (let index = 1; index <= 12; index++) {
            month_marks[index] = moment(`2021-${index}-1`).format('MMM')
        }
        let day_marks = {}
        for (let index = 1; index <= dateFiltered.daysInMonth(); index++) {
            day_marks[index] = index
        }

        return (
            <div className="leaflet-control-container">
                <div
                    className={'leaflet-control'}
                    style={{
                        height: 'auto',
                        width: '50%',
                        backgroundColor: '#0000',
                        left: '50%',
                        bottom: 0,
                        position: 'absolute',
                        transform: 'translate(-50%, 0%)',
                        margin: 0
                    }}>
                    <Drawer
                        title={`Select Date Range: ${dateFiltered.format('YYYY-MM-DD')}`}
                        height={280}
                        headerStyle={{
                            paddingBottom: 8
                        }}
                        maskStyle={{
                            backgroundColor: '#fff0'
                        }}
                        placement={'bottom'}
                        closable={true}
                        getContainer={false}
                        onClose={() => {
                            this.setState({ isShowBottomDrawer: false });
                            this
                                .mapRef
                                .current
                                .dragging
                                .enable();
                        }}
                        visible={isShowBottomDrawer}>

                        <div
                            style={{
                                marginTop: -20
                            }}>{`Year: `}</div>
                        <Slider
                            marks={year_marks}
                            min={years[0]}
                            max={years[1]}
                            included={false}
                            value={dateFiltered.year()}
                            onChange={(value) => {
                                const currentDate = `${value}-${dateFiltered.month() + 1}-${dateFiltered.date()}`;
                                this.setState({
                                    dateFiltered: moment(currentDate)
                                }, () => {
                                    this.updateTextureOverlay_2();
                                    this.updateBeacon_2();
                                });
                                if (this.state.unSyncKey['Date Slider'] || !this.state.isSync)
                                    return;
                                socket.emit('CHANGE_DATE_RANGE', currentDate);
                                socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                    xrange: [
                                        moment(currentDate)
                                            .add(-30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss'),
                                        moment(currentDate)
                                            .add(30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss')
                                    ]
                                })
                            }} />
                        <div>{`Month: `}</div>
                        <Slider
                            marks={month_marks}
                            min={1}
                            max={12}
                            included={false}
                            value={dateFiltered.month() + 1}
                            onChange={(value) => {
                                const currentDate = `${dateFiltered.year()}-${value}-${dateFiltered.date()}`;
                                this.setState({
                                    dateFiltered: moment(currentDate)
                                }, () => {
                                    this.updateBeacon_2();
                                });
                                if (this.state.unSyncKey['Date Slider'] || !this.state.isSync)
                                    return;
                                socket.emit('CHANGE_DATE_RANGE', currentDate);
                                socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                    xrange: [
                                        moment(currentDate)
                                            .add(-30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss'),
                                        moment(currentDate)
                                            .add(30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss')
                                    ]
                                })
                            }} />
                        <div>{`Day: `}</div>
                        <Slider
                            marks={day_marks}
                            min={1}
                            max={dateFiltered.daysInMonth()}
                            included={false}
                            value={dateFiltered.date()}
                            onChange={(value) => {
                                const currentDate = `${dateFiltered.year()}-${dateFiltered.month() + 1}-${value}`;
                                this.setState({
                                    dateFiltered: moment(currentDate)
                                }, () => {
                                    this.updateBeacon_2();
                                });
                                if (this.state.unSyncKey['Date Slider'] || !this.state.isSync)
                                    return;
                                socket.emit('CHANGE_DATE_RANGE', currentDate);
                                socket.emit('CHANGE_DATE_XRANGE_SLIDER', {
                                    xrange: [
                                        moment(currentDate)
                                            .add(-30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss'),
                                        moment(currentDate)
                                            .add(30, 'd')
                                            .format('YYYY-MM-DD HH:mm:ss')
                                    ]
                                })
                            }} />
                    </Drawer>
                </div>
            </div>
        )
    }

    onDragStart = () => {
        this.setState({
            activeDrags: ++this.state.activeDrags
        });
        this
            .mapRef
            .current
            .dragging
            .disable();
    };

    onDragStop = () => {
        this.setState({
            activeDrags: --this.state.activeDrags
        });
        this
            .mapRef
            .current
            .dragging
            .enable();
    };

    renderLegend(name, legendUrl) {
        const dragHandlers = {
            onStart: this.onDragStart,
            onStop: this.onDragStop
        };
        const {
            availableDataLayerKey,
            availableLegendKey,
            hideLegendKey,
            sortedTextureItems,
            lastProcessStationList,
            textureList
        } = this.state
        console.log('lastProcessStationList', lastProcessStationList);
        const stationTypeList = {}
        lastProcessStationList.forEach(item => {
            stationTypeList[item.typeName] = true
        })
        const filtered = sortedTextureItems.map(item => textureList.find(tx => tx.objectId === item)).reverse()
        console.log('availableLegendKey KKKKK', availableLegendKey);
        console.log('filtered KKKKK', filtered);

        return (
            <div className={'leaflet-control-container'}>
                <div className={'leaflet-control'}>
                    {DATA_LAYER_LIST.map(item => {
                        return availableDataLayerKey[item.name] && availableLegendKey[item.name]
                            ? (<LegendItem
                                onClose={() => {
                                    delete availableLegendKey[item.name];
                                    this.setState({ availableLegendKey })
                                }}
                                onHide={() => {
                                    if (hideLegendKey[item.name]) {
                                        delete hideLegendKey[item.name];
                                    } else {
                                        hideLegendKey[item.name] = true;
                                    }
                                    this.setState({ hideLegendKey });
                                }}
                                isHide={hideLegendKey[item.name]}
                                dragHandlers={dragHandlers}
                                legendUrl={item.legendUrl
                                    ? item.legendUrl.default
                                    : ''}
                                name={item.name} />)
                            : null
                    })}
                    {filtered.map(layer => {
                        const { children } = layer || {}
                        const item = children && children.length > 0 ? children[0] : null
                        return item && availableLegendKey[item.objectId] ? (<LegendItem
                            onClose={() => {
                                delete availableLegendKey[item.objectId];
                                this.setState({ availableLegendKey })
                            }}
                            onHide={() => {
                                if (hideLegendKey[item.objectId]) {
                                    delete hideLegendKey[item.objectId];
                                } else {
                                    hideLegendKey[item.objectId] = true;
                                }
                                this.setState({ hideLegendKey });
                            }}
                            isHide={hideLegendKey[item.objectId]}
                            dragHandlers={dragHandlers}
                            legendUrl={item.legendUrl
                                ? item.legendUrl
                                : ''}
                            name={`${item.typeName}-${item.countryName}`} />)
                            : null
                    })
                    }
                    {Object.keys(stationTypeList)
                        .map((item) => {
                            return item && availableLegendKey[item]
                                ? (<LegendItem
                                    onClose={() => {
                                        delete availableLegendKey[item];
                                        this.setState({ availableLegendKey })
                                    }}
                                    onHide={() => {
                                        if (hideLegendKey[item]) {
                                            delete hideLegendKey[item];
                                        } else {
                                            hideLegendKey[item] = true;
                                        }
                                        this.setState({ hideLegendKey });
                                    }}
                                    isHide={hideLegendKey[item]}
                                    dragHandlers={dragHandlers}
                                    legendUrl={legends_beacon[item] || ''}
                                    name={`${item}`} />)
                                : null
                        })
                    }
                </div>
            </div>
        )
    }
    renderClip(name, legendUrl) {
        const dragHandlers = {
            onStart: this.onDragStart,
            onStop: this.onDragStop
        };
        const {
            availableClipKey,
            hideClipKey,
            sortedTextureItems,
            textureList
        } = this.state

        const filtered = sortedTextureItems.map(item => textureList.find(tx => tx.objectId === item)).reverse()
        console.log('availableClipKey KKKKK', availableClipKey);
        console.log('filtered KKKKK', filtered);

        return (
            <div className={'leaflet-control-container'}>
                <div className={'leaflet-control'}>

                    {filtered.map(layer => {
                        const { children } = layer || {}
                        const item = children && children.length > 0 ? children[0] : null
                        return item && availableClipKey[item.objectId] ? (<ClipItem
                            onClose={() => {
                                delete availableClipKey[item.objectId];
                                this.setState({ availableClipKey })
                            }}
                            onHide={() => {
                                if (hideClipKey[item.objectId]) {
                                    delete hideClipKey[item.objectId];
                                } else {
                                    hideClipKey[item.objectId] = true;
                                }
                                this.setState({ hideClipKey });
                            }}
                            isHide={hideClipKey[item.objectId]}
                            dragHandlers={dragHandlers}
                            src={item.fileClip && item.fileClip[0]
                                ? item.fileClip[0].url
                                : ''}
                            name={`${item.typeName}-${item.countryName}`} />)
                            : null
                    })
                    }

                </div>
            </div>
        )
    }
    renderSyncButton() {
        return (
            <Button
                className={'leaflet-control'}
                variant="secondary"
                size="sm"
                style={{
                    width: 96,
                    height: 32,
                    marginBottom: 8
                }}
                onClick={() => {
                    const s = !this.state.isSync;
                    this.setState({ isSync: s });
                    if (this.state.unSyncKey['Global Sync'])
                        return;
                    socket.emit('SYNC', s);
                }}>
                {this.state.isSync
                    ? 'Un-sync'
                    : 'Sync'}
            </Button>
        )
    }
    renderSiteSelection() {
        const { siteChannel } = this.state;
        return (
            <DropdownButton
                style={{
                    marginRight: 16,
                    marginTop: 0,
                    marginBottom: 8
                }}
                className={'leaflet-control'}
                size="sm"
                title="Site Selection"
                onClick={async () => {
                    const sites = await (await axios(`${API_URL}/data/site`)).data
                    this.setState({ siteChannel: sites.data })
                }}
                variant="secondary">
                {[...SITE, ...siteChannel].map(item => (
                    <Dropdown.Item
                        onClick={() => {
                            this.onChangeSite(item)
                        }}>{item}</Dropdown.Item>
                ))}
            </DropdownButton>
        )
    }
    renderCreateSite() {
        return (
            <Input.Group compact>
                <Input style={{ width: 'calc(100% - 120px)' }} value={this.state.channelInput} onChange={(v) => {
                    this.setState({ channelInput: v.target.value })
                }} />
                <Button
                    variant="secondary"
                    size="sm"
                    onClick={async () => {
                        const res = await axios.post(`${API_URL}/data/site`, { name: this.state.channelInput })
                        console.log('res', res.data);
                        socket.emit('JOIN', this.state.channelInput)
                        this.setState({ currentSite: this.state.channelInput }, () => {
                            this.setState({ channelInput: '' })
                        })
                    }}>
                    {'Create or Join'}
                </Button>
            </Input.Group>
        )
    }
    renderPulseButton() {
        return (
            <Button
                className={'leaflet-control'}
                variant="secondary"
                size="sm"
                style={{
                    width: 160,
                    height: 32,
                    marginBottom: 8
                }}
                onClick={() => {
                    if (!this.state.pulseMarkerMode) {
                        message.info('Click on the map to create Pulse Marker')
                    }
                    this.setState({ isShowSettingDrawer: false, pulseMarkerMode: !this.state.pulseMarkerMode })

                }}>
                {`Pulse Marker: ${this.state.pulseMarkerMode ? 'ON' : 'OFF'}`}
            </Button>
        )
    }
    renderFullScreenButton() {
        return (
            <Button
                className={'leaflet-control'}
                variant="secondary"
                size="sm"
                style={{
                    width: 96,
                    height: 32,
                    marginBottom: 8
                }}
                onClick={() => {
                    const elem = document.getElementById('MAIN');
                    if (this.state.isFullScreen) {
                        closeFullscreen();
                        this.setState({ isFullScreen: false });
                    } else {
                        openFullscreen(elem);
                        this.setState({ isFullScreen: true });
                    }
                }}>
                {this.state.isFullScreen
                    ? 'Exit Full Screen'
                    : 'Full Screen'}
            </Button>
        )
    }
    renderDateSliderButton() {
        return (
            <Button
                className={'leaflet-control'}
                variant="secondary"
                size="sm"
                style={{
                    width: 160,
                    height: 32,
                    marginBottom: 8
                }}
                onClick={() => {
                    this.setState({
                        isShowBottomDrawer: !this.state.isShowBottomDrawer
                    });
                    this
                        .mapRef
                        .current
                        .dragging
                        .disable()
                }}>
                {'Data Time Selection'}
            </Button>
        )
    }
    renderLayerSortDrawerButton() {
        return (
            <Button
                className={'leaflet-control'}
                variant="secondary"
                size="sm"
                style={{
                    width: 180,
                    height: 32,
                    marginBottom: 8
                }}
                onClick={() => {
                    this.setState({
                        isShowLayerDrawer: !this.state.isShowLayerDrawer
                    });
                }}>
                {'Texture Sort'}
            </Button>
        )
    }
    renderZoomSlider = () => {
        let marks = {}
        for (let index = 0; index <= 18; index++) {
            marks[index] = index
        }
        return (
            <div>
                <div style={{ marginTop: 12, marginBottom: 8 }}>{'Map Zoom Level: '}</div>
                <Slider
                    marks={marks}
                    min={0}
                    max={18}
                    included={false}
                    value={this.state.zoom}
                    onChange={(zoom) => {
                        console.log('zoom', zoom);
                        const center = this.mapRef.current.getCenter()
                        this.setState({ zoom, lat: center.lat, lng: center.lng }, () => {
                            console.log('ceneter', zoom);
                            this.mapRef.current.setView(center, zoom)
                            if (this.state.unSyncKey['Location'] || !this.state.isSync)
                                return;
                            socket.emit('MAP_ZOOM', { center, zoom });
                        })
                    }} />
            </div>
        )
    }
    renderZoomSliderVerticle = () => {
        let marks = {}
        for (let index = 0; index <= 18; index++) {
            marks[index] = index
        }
        return (
            <div onMouseEnter={() => {
                console.log('ENTERRRR');
                this.mapRef.current.dragging.disable()
            }}
                onMouseLeave={() => {
                    console.log('OUT');
                    if (this.state.isShowBottomDrawer === false) {
                        this.mapRef.current.dragging.enable()
                    }
                }}
                style={{ width: 30, height: 400, marginBottom: 90, borderRadius: 6, padding: 4, marginRight: 10, paddingBottom: 16 }}>
                <Slider
                    className={'leaflet-control'}
                    vertical
                    min={0}
                    max={18}
                    included={false}
                    value={this.state.zoom}
                    onChange={(zoom) => {
                        console.log('zoom', zoom);
                        const center = this.mapRef.current.getCenter()
                        this.setState({ zoom, lat: center.lat, lng: center.lng }, () => {
                            console.log('ceneter', zoom);
                            this.mapRef.current.setView(center, zoom)
                            if (this.state.unSyncKey['Location'] || !this.state.isSync)
                                return;
                            socket.emit('MAP_ZOOM', { center, zoom });
                        })
                    }} />
            </div>

        )
    }
    renderSettingDrawer = () => {
        const { isShowSettingDrawer } = this.state
        return (
            <Drawer
                title="Map Setting"
                placement={'left'}
                width={400}
                closable={true}
                onClose={() => {
                    this.setState({ isShowSettingDrawer: false })
                }}
                visible={isShowSettingDrawer}
            >
                <div
                    style={{
                        backgroundColor: 'white',
                        width: 96,
                        height: 32,
                        textAlign: 'center',
                        borderRadius: 4,
                        fontSize: 10,
                        padding: 8
                    }}>{`Site : ${this.state.currentSite}`}</div>
                {this.renderCreateSite()}
                <Row gutter={8} style={{ marginTop: 12 }}>
                    <Col>
                        {this.renderSyncButton()}
                    </Col>
                    <Col>
                        {this.renderFullScreenButton()}
                    </Col>
                    <Col>
                        {this.renderDateSliderButton()}
                    </Col>
                    <Col>
                        {this.renderLayerSortDrawerButton()}
                    </Col>
                    <Col>
                        {this.renderPulseButton()}
                    </Col>
                    <Col>
                        {this.renderSiteSelection()}
                    </Col>

                </Row>
                {this.renderZoomSlider()}
                {this.renderUnSyncPanel()}
                {this.renderDataLayerPanel()}
                {this.renderBeaconPanel()}
                {this.renderTexturePanel()}
                {this.renderCountryFilterPanel()}
                <div style={{ fontSize: 14, marginTop: 24, textAlign: 'center' }}>{`Landsage4 Map Version ${VERSION}`}</div>
            </Drawer>
        )
    }

    onDragEnd(result) {
        if (!result.destination) {
            return;
        }
        const sortedTextureItems = reorder(this.state.sortedTextureItems, result.source.index, result.destination.index);
        console.log('sortedTextureItems', sortedTextureItems);
        this.setState({ sortedTextureItems }, () => {
            this.updateTextureOverlay_2()
            if (this.state.unSyncKey['Sort Texture'] || !this.state.isSync)
                return;
            socket.emit('SORTED_TEXTURE', sortedTextureItems);
        });
    }

    onDragEndBookmark(result) {
        if (!result.destination) {
            return;
        }
        const sortedBookmarkItems = reorder(this.state.bookmarkLocations, result.source.index, result.destination.index);
        console.log('sortedBookmarkItems', sortedBookmarkItems);
        this.setState({ bookmarkLocations: sortedBookmarkItems }, () => {
            localStorage.setItem('bookmarkLocations', JSON.stringify(sortedBookmarkItems))
        });

    }


    renderLayerDrawer = () => {
        const { isShowLayerDrawer, sortedTextureItems, textureList, availableLegendKey, availableClipKey } = this.state
        const filtered = sortedTextureItems.map(item => textureList.find(tx => tx.objectId === item))
        return (
            <Drawer
                title="Texture Sort"
                placement={'right'}
                closable={true}
                onClose={() => {
                    this.setState({ isShowLayerDrawer: false })
                }}
                visible={isShowLayerDrawer}
            >
                <div style={{ marginBottom: 8 }}>{'Texture Sorting'}</div>
                <DragDropContext onDragEnd={this.onDragEnd.bind(this)}>
                    <div class="pulsating-circle"></div>
                    <Droppable droppableId="droppable">
                        {(provided, snapshot) => (
                            <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                                style={getListStyle(snapshot.isDraggingOver)}
                            >
                                {filtered.map((item, index) => (
                                    <DraggableItem key={item.objectId} draggableId={item.objectId} index={index}>
                                        {(provided, snapshot) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                style={getItemStyle(
                                                    snapshot.isDragging,
                                                    provided.draggableProps.style
                                                )}
                                            >
                                                <Row>
                                                    <Col span={18}>
                                                        {`[${item.typeName}] [${item.areaName}] ${item.name}`}
                                                    </Col>
                                                    <Col span={6}>
                                                        <Checkbox checked={availableLegendKey[item.children[0].objectId]} onChange={(e) => {
                                                            if (item.children[0]) {
                                                                if (!e.target.checked) {
                                                                    delete availableLegendKey[item.children[0].objectId];
                                                                } else {
                                                                    availableLegendKey[item.children[0].objectId] = true
                                                                }
                                                                this.setState({ availableLegendKey })
                                                            }

                                                        }}>{'Legend'}</Checkbox>
                                                        {
                                                            item.children[0].fileClip && item.children[0].fileClip[0] && (
                                                                <Checkbox checked={availableClipKey[item.children[0].objectId]} onChange={(e) => {
                                                                    if (item.children[0]) {
                                                                        if (!e.target.checked) {
                                                                            delete availableClipKey[item.children[0].objectId];
                                                                        } else {
                                                                            availableClipKey[item.children[0].objectId] = true
                                                                        }
                                                                        this.setState({ availableClipKey })
                                                                    }

                                                                }}>{'Video'}</Checkbox>
                                                            )
                                                        }
                                                    </Col>
                                                </Row>

                                            </div>
                                        )}
                                    </DraggableItem>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>

            </Drawer>
        )
    }

    renderBookmarkDrawer = () => {
        const { isShowBookmarkDrawer, bookmarkLocations } = this.state
        return (
            <Drawer
                title="Bookmark Locations"
                placement={'right'}
                closable={true}
                onClose={() => {
                    this.setState({ isShowBookmarkDrawer: false })
                }}
                visible={isShowBookmarkDrawer}
            >
                <div style={{ marginBottom: 8 }}>{'Location Search'}</div>
                <GooglePlacesAutocomplete selectProps={{
                    value: this.state.searchLocation,
                    onChange: async (geo) => {
                        console.log('geo', geo);
                        const res = await geocodeByPlaceId(geo.value.place_id)
                        if (res.length > 0) {
                            const center = { lat: res[0].geometry.location.lat(), lng: res[0].geometry.location.lng() }
                            this.mapRef.current.setView(center, this.state.zoom)
                            this.pulsing({ latlng: center })
                            if (this.state.unSyncKey['Pulse Marker'] || !this.state.isSync)
                                return;
                            socket.emit('PULSE_MARKER', center)
                        }
                    },
                }} />
                <div style={{ marginTop: 16, marginBottom: 8 }}>{'Please double click on the map to create location bookmark...'}</div>
                <DragDropContext onDragEnd={this.onDragEndBookmark.bind(this)}>
                    <div class="pulsating-circle"></div>
                    <Droppable droppableId="droppable_bookmark">
                        {(provided, snapshot) => (
                            <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                                style={getListStyle(snapshot.isDraggingOver)}
                            >
                                {bookmarkLocations.map((item, index) => (
                                    <DraggableItem key={item.objectId} draggableId={item.objectId} index={index}>
                                        {(provided, snapshot) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                style={getItemStyle(
                                                    snapshot.isDragging,
                                                    provided.draggableProps.style
                                                )}
                                                onClick={() => {
                                                    this.mapRef.current.setView(item.location, this.state.zoom)
                                                    this.pulsing({ latlng: item.location })
                                                }}
                                            >
                                                <a href="javascript:void(0)" onClick={() => {
                                                    const arr = this.state.bookmarkLocations
                                                    const filterBookmarks = arr.filter(a => a.objectId !== item.objectId)
                                                    this.setState({ bookmarkLocations: filterBookmarks }, () => {
                                                        localStorage.setItem('bookmarkLocations', JSON.stringify(filterBookmarks))
                                                        message.success('Bookmark Location Removed..')
                                                    })

                                                }} style={{ color: 'red', position: 'absolute', right: 42, textAlign: 'right', fontSize: 12 }}>{'remove'}</a>
                                                <div>{item.name}</div>
                                                <div>{`[${item.location.lat.toFixed(6)},${item.location.lng.toFixed(6)}]`}</div>
                                            </div>
                                        )}
                                    </DraggableItem>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>

            </Drawer>
        )
    }


    pulsing = ({ latlng }) => {
        const pulseMark = new Leaflet.marker(latlng, { icon: PulseIcon }).addTo(this.mapRef.current)
        setTimeout(() => {
            pulseMark.removeFrom(this.mapRef.current)
        }, 3800)
    }


    render() {
        const position = [this.state.lat, this.state.lng]
        const { dataLayerList, lastProcessStationList, unSyncKey } = this.state
        console.log('xxdataLayerList', dataLayerList)
        return (
            <div id="MAIN">
                <MapContainer
                    whenCreated={mapInstance => {
                        this.mapRef.current = mapInstance;
                        this.mapRef.current.on('click', (e) => {
                            if (this.state.pulseMarkerMode) {
                                console.log('e', e);
                                this.pulsing({ latlng: e.latlng })
                                if (this.state.unSyncKey['Pulse Marker'] || !this.state.isSync)
                                    return;
                                socket.emit('PULSE_MARKER', e.latlng)
                            }
                        })
                        this.mapRef.current.on('dblclick', (e) => {
                            console.log('dblclick e', e);
                            let name = ''
                            const that = this
                            const popup = Modal.confirm({
                                title: 'Bookmark Name',
                                content: (
                                    <div>
                                        <p>{'Enter Location Name...'}</p>
                                        <Input onChange={(v) => {
                                            name = v.target.value
                                        }} />
                                    </div>
                                ),
                                onOk() {
                                    const arr = that.state.bookmarkLocations
                                    arr.push({ name, objectId: `${Date.now()}`, location: e.latlng })
                                    localStorage.setItem('bookmarkLocations', JSON.stringify(arr))
                                    that.setState({ bookmarkLocations: arr })
                                    message.success('Bookmark Location Saved..')
                                },
                                onCancel() {
                                }
                            });
                        })
                        this.mapRef.current.on('drag', (e) => {
                            if (this.state.unSyncKey['Location'] || !this.state.isSync)
                                return;
                            socket.emit('MAP_CAMERA', e.target.getCenter());
                        });
                        this.mapRef.current.on('zoomend', (e) => {
                            // console.log('e', e);
                            // if (unSyncKey['Location'] || this.state.selfZoom)
                            //     return;
                            // socket.emit('MAP_ZOOM', { center: e.target.getCenter(), zoom: e.target.getZoom(), fromId: socket.id });
                        })
                    }}
                    center={position}
                    {...disableZoom}
                    zoom={this.state.zoom}
                    style={{
                        height: '100vh'
                    }}>
                    <LayersControl position="bottomright">
                        <LayersControl.BaseLayer checked name="OpenStreetMap">
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url={normalUrl} />
                        </LayersControl.BaseLayer>
                        <LayersControl.BaseLayer name="ErsiWorldImagery">
                            <TileLayer
                                attribution='&copy; <a href="https://www.esri.com/">Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community</a> contributors'
                                url={satelliteUrl} />
                        </LayersControl.BaseLayer>
                    </LayersControl>
                    {Object
                        .keys(dataLayerList)
                        .map(layer => {
                            console.log('layer', layer);
                            const layerItem = DATA_LAYER_LIST.find(obj => obj.name === layer);
                            // console.log(layerItem);
                            // console.log(layerItem.icon);
                            return dataLayerList[layer].map(item => {
                                // console.log("xxxxxxxxxxxxx ", item);
                                this.markerRefs[item.id] = React.createRef();
                                if(layer === "Water Level Station Of National Hydroinformatics Data Center" || layer === "Rainfall Station Of National Hydroinformatics Data Center" || layer === "RainfallTMD" || layer === "Landslide Assessment"){
                                // if(layer === "Water Level Station Of National Hydroinformatics Data Center" || layer === "Rainfall Station Of National Hydroinformatics Data Center" || layer === "RainfallTMD"){
                                    return (<DataMarkerNHDC
                                        markerRef={(ref) => {
                                            this.markerRefs[item.id] = ref
                                        }}
                                        position={item.position}
                                        // id={item.id}
                                        stationName={item.name}
                                        icon={layer === "Water Level Station Of National Hydroinformatics Data Center" ? 
                                            layerItem.icon(item.Percent, item['Water Level'], item.WaterLevelPrev): (layer === "Landslide Assessment" ? 
                                            layerItem.icon(item.isRisk): layerItem.icon(item.Rain))}
                                        // icon={layer === "Water Level Station Of National Hydroinformatics Data Center" ? 
                                        //     layerItem.icon(item.Percent, item['Water Level'], item.WaterLevelPrev): layerItem.icon(item.Rain)}
                                        data={item}
                                        detailKeys={layerItem.detailKeys}
                                        vizPath={layerItem.path}
                                        onClick={{
                                            click: (event) => this.handleClickMarker(event, item.id)
                                        }} />)
                                }else{
                                    return (<DataMarker
                                        markerRef={(ref) => {
                                            this.markerRefs[item.id] = ref
                                        }}
                                        position={item.position}
                                        id={item.id}
                                        icon={layerItem.icon}
                                        data={item}
                                        detailKeys={layerItem.detailKeys}
                                        vizPath={layerItem.path}
                                        onClick={{
                                            click: (event) => this.handleClickMarker(event, item.id)
                                        }} />)
                                }
                            })
                        })}
                    {lastProcessStationList
                        .map(item => {
                            this.markerRefs[item.objectId] = React.createRef();
                            return (<BeaconMarker
                                markerRef={(ref) => {
                                    this.markerRefs[item.objectId] = ref
                                }}
                                site={this.state.currentSite}
                                position={item.position}
                                id={item.objectId}
                                name={item.name}
                                sortedTimeSeriesData={item.sortedTimeSeriesData}
                                data={item}
                                icon={item.renderIcon}
                                onClick={{
                                    click: (event) => this.handleClickMarker(event, item.objectId)
                                }} />)
                        })}
                    <div className="leaflet-control-container">
                        <div className="leaflet-top leaflet-left">
                            <div>
                                <Button
                                    className={'leaflet-control'}
                                    variant="secondary"
                                    size="sm"
                                    style={{
                                        width: 72,
                                        height: 32,
                                        marginRight: 1
                                    }}
                                    onClick={() => {
                                        this.setState({ isShowSettingDrawer: true })
                                    }}>
                                    {'Setting'}
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="leaflet-control-container">
                        <div className="leaflet-top leaflet-right">
                            <div>
                                <Button
                                    className={'leaflet-control'}
                                    variant="secondary"
                                    size="sm"
                                    style={{
                                        width: 120,
                                        height: 32,
                                        marginTop: 12, marginBottom: 8
                                    }}
                                    onClick={() => {
                                        this.setState({
                                            isShowLayerDrawer: !this.state.isShowLayerDrawer
                                        });
                                    }}>
                                    {'Texture Sort'}
                                </Button>
                            </div>
                        </div>
                        <div className="leaflet-top leaflet-right">
                            <div>
                                <Button
                                    className={'leaflet-control'}
                                    variant="secondary"
                                    size="sm"
                                    style={{
                                        width: 180,
                                        height: 32,
                                        marginTop: 56, marginBottom: 8
                                    }}
                                    onClick={() => {
                                        this.setState({
                                            isShowBookmarkDrawer: !this.state.isShowBookmarkDrawer
                                        });
                                    }}>
                                    {'Bookmark & Search'}
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="leaflet-control-container">
                        <div className="leaflet-bottom leaflet-right ">
                            {this.renderZoomSliderVerticle()}
                        </div>
                    </div>

                    {this.renderBottomDrawer()}
                    {this.renderLegend()}
                    {this.renderClip()}
                    {this.renderSettingDrawer()}
                    {this.renderLayerDrawer()}
                    {this.renderBookmarkDrawer()}
                </MapContainer>
            </div>
        )
    }
}

const disableZoom = {
    zoomControl: false,
    doubleClickZoom: false,
    zoomSnap: true,
    zoomDelta: false,
    trackResize: true,
    touchZoom: false,
    scrollWheelZoom: false
}

const LegendItem = ({
    name,
    legendUrl,
    dragHandlers,
    onClose,
    onHide,
    isHide
}) => {
    const random = Math.floor(Math.random() * 60)

    return (
        <Draggable
            grid={[10, 10]}
            handle="#dragBar"
            {...dragHandlers}
            defaultPosition={{
                x: 25 + random,
                y: 400 + random
            }}>
            <div
                className="leaflet-control"
                style={{
                    backgroundColor: 'white',
                    position: 'absolute',
                    boxShadow: '2px 6px 8px 1px #0003',
                    width: isHide
                        ? 100
                        : 240,
                    height: isHide
                        ? 25
                        : 280
                }}>
                <div
                    id="dragBar"
                    onClick={() => {
                        console.log('dd')
                    }}
                    style={{
                        height: 25,
                        width: '100%',
                        backgroundColor: '#F0F0F0',
                        cursor: 'move',
                        overflowWrap: 'normal',
                        zIndex: 10000 + random
                    }}>
                    <Row style={{
                        marginLeft: 0
                    }}>
                        <div
                            onClick={onClose}
                            style={{
                                paddingTop: 4,
                                height: 25,
                                width: 25,
                                backgroundColor: '#EF4B4B',
                                textAlign: 'center',
                                cursor: 'pointer',
                                color: '#fff',
                                fontSize: '30',
                                fontWeight: 'bold'
                            }}>X</div>
                        <div
                            onClick={onHide}
                            style={{
                                paddingTop: 4,
                                height: 25,
                                width: 25,
                                backgroundColor: '#F9B208',
                                textAlign: 'center',
                                cursor: 'pointer',
                                color: '#fff',
                                fontSize: '30',
                                fontWeight: 'bold'
                            }}>-</div>
                        <div
                            style={{
                                textAlign: 'center',
                                fontWeight: 'bold',
                                marginTop: 4,
                                marginLeft: 8
                            }}>{name}</div>
                    </Row>
                    {!isHide
                        ? (
                            <div
                                style={{
                                    justifyContent: 'center',
                                    textAlign: 'center',
                                    padding: 8
                                }}>
                                <img
                                    src={legendUrl}
                                    style={{
                                        width: 200,
                                        height: 200,
                                        // width: 220,
                                        // height: 220,
                                        objectFit: 'contain'
                                    }} />
                            </div>
                        )
                        : null}
                </div>
            </div>
        </Draggable>
    )
}


const ClipItem = ({
    name,
    src,
    dragHandlers,
    onClose,
    onHide,
    isHide
}) => {
    const random = Math.floor(Math.random() * 60)

    return (
        <Draggable
            grid={[10, 10]}
            handle="#dragBar"
            {...dragHandlers}
            defaultPosition={{
                x: 25 + random,
                y: 400 + random
            }}>
            <div
                className="leaflet-control"
                style={{
                    backgroundColor: 'white',
                    position: 'absolute',
                    boxShadow: '2px 6px 8px 1px #0003',
                    width: isHide
                        ? 100
                        : 360,
                    height: isHide
                        ? 25
                        : 360
                }}>
                <div
                    id="dragBar"
                    onClick={() => {
                        console.log('dd')
                    }}
                    style={{
                        height: 25,
                        width: '100%',
                        backgroundColor: '#F0F0F0',
                        cursor: 'move',
                        overflowWrap: 'normal',
                        zIndex: 10000 + random
                    }}>
                    <Row style={{
                        marginLeft: 0
                    }}>
                        <div
                            onClick={onClose}
                            style={{
                                paddingTop: 4,
                                height: 25,
                                width: 25,
                                backgroundColor: '#EF4B4B',
                                textAlign: 'center',
                                cursor: 'pointer',
                                color: '#fff',
                                fontSize: '30',
                                fontWeight: 'bold'
                            }}>X</div>
                        <div
                            onClick={onHide}
                            style={{
                                paddingTop: 4,
                                height: 25,
                                width: 25,
                                backgroundColor: '#F9B208',
                                textAlign: 'center',
                                cursor: 'pointer',
                                color: '#fff',
                                fontSize: '30',
                                fontWeight: 'bold'
                            }}>-</div>
                        <div
                            style={{
                                textAlign: 'center',
                                fontWeight: 'bold',
                                marginTop: 4,
                                marginLeft: 8
                            }}>{name}</div>
                    </Row>
                    {!isHide
                        ? (
                            <div
                                style={{
                                    justifyContent: 'center',
                                    textAlign: 'center',
                                    padding: 8
                                }}>
                                <video width="320" height="320" autoplay controls>
                                    <source src={src} type="video/mp4" />
                                    Your browser does not support the video tag.
                                </video>
                            </div>
                        )
                        : null}
                </div>
            </div>
        </Draggable>
    )
}

const getItems = count =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${k}`,
        content: `item ${k}`
    }));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    fontSize: 12,
    padding: grid * 1.2,
    margin: `0 0 ${grid}px 0`,
    border: `1px solid #f0f0f0`,
    // change background colour if dragging
    background: isDragging ? "#bfffe3" : "#fff",
    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#bfeaff" : "#fff",
    padding: grid,
    width: '100%'
});

