#!/bin/sh
serve -s build -l 4000 --ssl-cert /etc/letsencrypt/live/landsage.app/fullchain.pem --ssl-key /etc/letsencrypt/live/landsage.app/privkey.pem --config ../serve.json